extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	$"/root/HUD/Panel".visible = false;
	$"/root/HUD/Panel/Sprite".visible = false;
	$"/root/HUD/Panel/Label".visible = false;
	$"/root/HUD".coin = 0
	$"/root/HUD/Panel2".visible = false;
	$"/root/HUD/Panel2/HP".visible = false;
	$"/root/HUD".hp = 3
	$AudioStreamPlayer2D.play()


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Button_pressed():
	$"/root/HUD/Panel".visible = true;
	$"/root/HUD/Panel/Sprite".visible = true;
	$"/root/HUD/Panel/Label".visible = true;
	$"/root/HUD".coin = 0
	$"/root/HUD/Panel2".visible = true;
	$"/root/HUD/Panel2/HP".visible = true;
	$"/root/HUD".hp = 3
	get_tree().change_scene("res://Level1.tscn")


func _on_Button2_pressed():
	get_tree().change_scene("res://HowToPlay.tscn")
