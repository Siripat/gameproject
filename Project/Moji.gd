extends KinematicBody2D

export var acceleraition = 500
export var max_speed = 50
export var friction = 600

enum{
	idle,
	chase
}



var speed_velocity = Vector2.ZERO
var state = idle

onready var animationPlayer = $AnimationPlayer
onready var animationTree = $AnimationTree
onready var animationState = animationTree.get("parameters/playback")
onready var Zone_See_player = $See_Zone

func _ready():
	animationTree.active = true
	animationState.travel("Sleepping")
	state = idle
	
func _process(delta):
	match state:
		idle:
			animationState.travel("Down")
			speed_velocity = speed_velocity.move_toward(Vector2.ZERO,friction * delta)
			Seek_player()
		chase:
			var player = Zone_See_player.player
			if player != null:
				var direction = (player.global_position-global_position).normalized()
				animationTree.set("parameters/Down/blend_position",direction)
				animationTree.set("parameters/Run/blend_position",direction)
				animationState.travel("Run")
					
				speed_velocity = speed_velocity.move_toward(direction * max_speed,acceleraition * delta)
			else:
				state = idle
				
	speed_velocity = move_and_slide(speed_velocity)
		
func Seek_player():
	if Zone_See_player.See_now():
		state = chase


func _on_Hurtbox_area_entered(area):
		queue_free()
		get_tree().change_scene("res://YouLose.tscn")
