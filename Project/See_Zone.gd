extends Area2D

var player = null

func See_now():
	return player != null

func _on_See_Zone_body_entered(body):
	player = body


func _on_See_Zone_body_exited(body):
	player = null
