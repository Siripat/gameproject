extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_coin_body_entered(body):
	if (body.name == "Player"):
		$"/root/HUD".coin += 1
		$AudioStreamPlayer2D.play()
		

func _on_AudioStreamPlayer2D_finished():
	queue_free()
