extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	$"/root/HUD/Panel".visible = false;
	$"/root/HUD/Panel/Sprite".visible = false;
	$"/root/HUD/Panel/Label".visible = false;
	$"/root/HUD".coin = 0
	$"/root/HUD/Panel2".visible = false;
	$"/root/HUD/Panel2/HP".visible = false;
	$"/root/HUD".hp = 3


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Button_pressed():
	get_tree().change_scene("res://MainScreen.tscn")
