extends KinematicBody2D

export var acceleraition = 500
export var max_speed = 90
export var friction = 600

var knockback = Vector2.ZERO
onready var stats = $Stats


enum{
	idle,
	chase
}

var speed_velocity = Vector2.ZERO
var state = idle

onready var Zone_See_player = $See_Zone

func _ready():
	print(stats.max_health)
	print(stats.health)
	state = idle
	
func _physics_process(delta):
	knockback = knockback.move_toward(Vector2.ZERO,200*delta)
	knockback = move_and_slide(knockback)
	
	match state:
		idle:
			
			speed_velocity = speed_velocity.move_toward(Vector2.ZERO,friction * delta)
			Seek_player()
		chase:
			var player = Zone_See_player.player
			if player != null:
				var direction = (player.global_position-global_position).normalized()
				speed_velocity = speed_velocity.move_toward(direction * max_speed,acceleraition * delta)
			else:
				state = idle
				
	speed_velocity = move_and_slide(speed_velocity)
		
func Seek_player():
	if Zone_See_player.See_now():
		state = chase

func _on_Hurtbox_area_entered(area):
	stats.health -= area.damage
	knockback = area.knockback_vector * 300
	print(knockback)

func _on_Stats_no_health():
	queue_free()



func _on_Area2D_area_entered(area):
	$"/root/HUD".hp -= 1
	$AudioStreamPlayer2D.play()
	if $"/root/HUD".hp == 0:
		get_tree().change_scene("res://YouLose.tscn")
	
