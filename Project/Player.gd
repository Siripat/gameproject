extends KinematicBody2D

const acceleraition = 600
const max_speed = 200
const roll_speed = 120
const friction = 800

enum {
	move,
	attack
}


var state = move
var speed_vector = Vector2.ZERO
var roll_vector = Vector2.DOWN
var stats = Playerstats

onready var animationPlayer = $AnimationPlayer
onready var animationTree = $AnimationTree
onready var animationState = animationTree.get("parameters/playback")
onready var SwHitbox = $HitboxHivot/SwHitbox
onready var Playerstats = $Playerstats

# Called when the node enters the scene tree for the first time.
func _ready():
	animationTree.active = true
	SwHitbox.knockback_vector = roll_vector

func _process(delta):
	
	
	match state:
		move:
			move_state(delta)
		attack:
			attack_state(delta)
		
		
func move_state(delta):
	var input_vector = Vector2.ZERO
	input_vector.x = Input.get_action_strength("ui_right")-Input.get_action_strength("ui_left")
	input_vector.y = Input.get_action_strength("ui_down")-Input.get_action_strength("ui_up")
	input_vector = input_vector.normalized()
	
	if input_vector != Vector2.ZERO:
		roll_vector = input_vector
		SwHitbox.knockback_vector = input_vector
		animationTree.set("parameters/Idle/blend_position",input_vector)
		animationTree.set("parameters/Walk/blend_position",input_vector)
		animationTree.set("parameters/Attack/blend_position",input_vector)
		animationState.travel("Walk")
		
		speed_vector = speed_vector.move_toward(input_vector * max_speed,acceleraition * delta)
	else:
		animationState.travel("Idle")
		speed_vector = speed_vector.move_toward(Vector2.ZERO ,friction * delta)
		
	speed_vector = move_and_slide(speed_vector)
		
	if Input.is_action_pressed("ui_attack"):
		state = attack
		
	
func attack_state(delta):
	animationState.travel("Attack")
		
func attack_end():
	state = move
	
